#!/usr/bin/make -f

.PHONY: all libpkg pkg

all : libpkg pkg

libpkg : 
	$(MAKE) -C libpkg


pkg :
	$(MAKE) -C pkg


clean :
	$(MAKE) -C libpkg clean
	$(MAKE) -C pkg clean
