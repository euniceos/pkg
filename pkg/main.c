/*
 * main.c
 *
 *  Created on: 23 Mar 2015
 *      Author: thebeast
 */



#include <stdlib.h>
#include <stdio.h>
#include <locale.h>
#include <unistd.h>
#include <getopt.h>

#include "../libpkg/libpkg.h"
#include "../libpkg/list.h"
#include "pkg.h"

int main(int argc, char** argv)
{
    bindtextdomain(PROGNAME, LOCALEDIR);
    textdomain(PROGNAME);

    runtime_t rt = new_runtime();

	int next_opt;
	const char* short_opts = SHORT_OPTS;
	const struct option long_opts[] =
	{
		{ INSTALL_LONG,		NOREQUIRED,		NULL,		INSTALL_SHORT     },
		{ REMOVE_LONG,		NOREQUIRED,		NULL,		REMOVE_SHORT      },
		{ UPDATE_LONG,		NOREQUIRED,		NULL,		UPDATE_SHORT      },
		{ CONFIG_LONG,		NOREQUIRED,		NULL,		CONFIG_SHORT      },
		{ PACKAGE_LONG,		NOREQUIRED,		NULL,		PACKAGE_SHORT     },
		{ EXTRACT_LONG,		NOREQUIRED,		NULL,		EXTRACT_SHORT     },
		{ NEWDB_LONG,		NOREQUIRED,		NULL,		NEWDB_SHORT       },
		{ QUERY_LONG,		REQUIRED,		NULL,		QUERY_SHORT       },
		{ HELP_LONG,		NOREQUIRED,		NULL,		HELP_SHORT        },
		{ DESTROOT_LONG,	REQUIRED,		NULL,		DESTROOT_SHORT    },
		{ VERBOSE_LONG,		NOREQUIRED,		NULL,		VERBOSE_SHORT     },
		{ SILENT_LONG,		NOREQUIRED,		NULL,		SILENT_SHORT      },
		{ NOSCRIPT_LONG,	NOREQUIRED,		NULL,		NOSCRIPT_SHORT    },
		{ NOTRIGGERS_LONG,	NOREQUIRED,		NULL,		NOTRIGGERS_SHORT  },
		{ NORUNDEPS_LONG,	NOREQUIRED,		NULL,		NORUNDEPS_SHORT   },
		{ NOCONFLICTS_LONG,	NOREQUIRED,		NULL,		NOCONFLICTS_SHORT },
		{ NOCONFIG_LONG,	NOREQUIRED,		NULL,		NOCONFIG_SHORT    },
		{ NOHOLD_LONG,		NOREQUIRED,		NULL,		NOHOLD_SHORT      },
		{ NULL,				NOREQUIRED,		NULL,		FALSE             },
	};

	opterr = 0;

	while(1)
	{
		next_opt = getopt_long(argc, argv, short_opts, long_opts, NULL);
		if(next_opt == -1)
			break;

		switch(next_opt)
		{
			case INSTALL_SHORT :
				rt.action = INSTALL;
				break;
			case REMOVE_SHORT :
				rt.action = REMOVE;
				break;
			case UPDATE_SHORT :
				rt.action = UPDATE;
				break;
			case CONFIG_SHORT :
				rt.action = CONFIG;
				break;
			case PACKAGE_SHORT :
				rt.action = PACKAGE;
				break;
			case EXTRACT_SHORT :
				rt.action = EXTRACT;
				break;
			case NEWDB_SHORT :
				rt.action = NEWDB;
				break;
			case QUERY_SHORT :
				rt.action = QUERY;
				rt.queryfields = (char*)optarg;
				break;
			case HELP_SHORT :
				rt.action = HELP;
				break;
			case DESTROOT_SHORT :
				rt.destroot= (char*)optarg;
				break;
			case VERBOSE_SHORT :
				rt.silent = FALSE;
				rt.verbose = TRUE;
				break;
			case SILENT_SHORT :
				rt.verbose = FALSE;
				rt.silent = TRUE;
				break;
			case NOSCRIPT_SHORT :
				rt.scripts = FALSE;
				break;
			case NOTRIGGERS_SHORT :
				rt.triggers = FALSE;
				break;
			case NORUNDEPS_SHORT :
				rt.rundeps = FALSE;
				break;
			case NOCONFLICTS_SHORT :
				rt.conflicts = FALSE;
				break;
			case NOCONFIG_SHORT :
				rt.config = FALSE;
				break;
			case NOHOLD_SHORT :
				rt.hold = FALSE;
				break;
			case '?' :
			{
				switch(optopt)
				{
					case DESTROOT_SHORT :
						printf(_("-%c option required an argument\n"), (char)optopt);
						break;
					case QUERY_SHORT :
						printf(_("-%c option required an argument\n"), (char)optopt);
						break;
					default :
						if(isprint(optopt))
						{
							printf(_("-%c unknow option\n"), (char)optopt);
						} else {
							printf(_("-%c unknow charset\n"), (char)optopt);
						}
				}
				exit(EXIT_FAILURE);
			}
		}
	}


	if ( (argc - optind) > 0 )
	{
		ushort_t i = optind;
		do
		{
			push_back(rt.pkgv, argv[i]);
			i++;
		} while(i < argc);
	}

	switch(rt.action)
	{
		case NONE :
			printf(_("No runtime action was defined\n"));
			exit(RETURN_FAILURE);
		case INSTALL :
			break;
		case REMOVE :
			break;
		case UPDATE :
			break;
		case CONFIG :
			break;
		case PACKAGE :
			break;
		case EXTRACT :
			break;
		case NEWDB :
			exit(exec_newdb_action(rt));
			break;
		case QUERY :
			break;
		case HELP :
			break;
		default :
			break;
	}

	exit(EXIT_SUCCESS);
}
