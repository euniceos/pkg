/*
 * newdb.c
 *
 *  Created on: 16/03/2015
 *      Author: thebeast
 */


#include <stdlib.h>

#include "../libpkg/libpkg.h"
#include "../libpkg/unixhandlers.h"
#include "../libpkg/unixtools.h"

#include "pkg.h"


ushort_t exec_newdb_action(runtime_t rt)
{
	//unix_file_t* base = new_unix_file(pkg_concat_str(5, " ", rt.destroot, DB_HOME, "/", "/"));

	//printf("rawpath :%s\nfullpath :%s\ndirname :%s\nbasename :%s\n", base->rawpath, base->fullpath, base->dirname, base->basename);

	path_t* pkg_home_path = new_path(pkg_concat_str(5, " ", rt.destroot, DB_HOME, "/", "/"));

	printf(
		"rawinput : %s\n"
		"exists   : %d\n"
		"isafile  : %d\n"
		"isadir   : %d\n"
		"error    : %d\n"
		"message  : %s\n", \
		pkg_home_path->rawinput, \
		pkg_home_path->exist, \
		pkg_home_path->isafile, \
		pkg_home_path->isadir, \
		pkg_home_path->error, \
		pkg_home_path->errmsg
	);

	//destroy_path(pkg_home_path);
	free(pkg_home_path);

	return RETURN_SUCCESS;
}
