/*
 * pkg.c
 *
 *  Created on: 15/03/2015
 *      Author: thebeast
 */



#include <stdlib.h>
#include <unistd.h>

#include "../libpkg/libpkg.h"
#include "../libpkg/list.h"
#include "pkg.h"


runtime_t new_runtime(void)
{
	runtime_t rt;

	switch((int)__WORDSIZE)
	{
		case 32 :
			rt.system.sysarch = BIT32;
			rt.system.pkgsarch = "i686";
			break;
		case 64 :
			rt.system.sysarch = BIT64;
			rt.system.pkgsarch = "x86_64";
			break;
		default :
			break;
	}

	rt.system.cpunum = sysconf(_SC_NPROCESSORS_ONLN);

	rt.action = NONE;
	rt.queryfields = "none";
	rt.destroot = "/";
	rt.verbose = FALSE;
	rt.silent = FALSE;
	rt.scripts = TRUE;
	rt.triggers = TRUE;
	rt.rundeps = TRUE;
	rt.conflicts = TRUE;
	rt.config = TRUE;
	rt.hold = TRUE;
	rt.pkgv = new_list();

	return rt;
}
