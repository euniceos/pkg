/*
 * unixtools.h
 *
 *  Created on: 20/03/2015
 *      Author: thebeast
 */


#ifndef UNIXTOOLS_H_
#define UNIXTOOLS_H_

char* pkg_concat_str(int count, ...);
char* pkg_strip_repeat(char* str, char ch);
char* pkg_strip_spaces(char* str);
char* pkg_strip_trailing(char* str);
char* pkg_get_realpath(char* str);
char* pkg_get_dirname(char* str);
char* pkg_get_basename(char* str);

#endif /* UNIXTOOLS_H_ */
