/*
 * list.c
 *
 *  Created on: 15/03/2015
 *      Author: thebeast
 */



#include <stdlib.h>
#include <stddef.h>
#include <string.h>

#include "libpkg.h"
#include "list.h"

list_t* new_list()
{
	list_t* list = malloc(sizeof(list_t));
	list->size = 0;
	list->vector = malloc(sizeof(void**));

	return list;
}

ushort_t push_back(list_t* list, void* member)
{
	size_t old = list->size;
	size_t new = old+1;
	size_t idx = new-1;

	void** new_vector = realloc(list->vector, new*sizeof(void*));
	list->vector = new_vector;
	list->size = new;
	list->vector[idx]=member;

	return RETURN_SUCCESS;
}

size_t get_size(list_t* list)
{
	return list->size;
}

void* get_position(list_t* list, size_t index)
{
	if (index < 0 || index >= list->size)
		return(NULL);

	return list->vector[index];
}

void destroy_list(list_t* list)
{
	free(list->vector);
	free(list);
}

void iter_list(list_t* list)
{
	size_t size = get_size(list);
	size_t iter = 0;

	if( size > 0 )
	{
		for( iter = 0; iter<size; iter++ )
			printf("%s\n", (char*)get_position(list, iter));
	}
}
