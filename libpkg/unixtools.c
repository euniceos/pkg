/*
 * unixtools.c
 *
 *  Created on: 16/03/2015
 *      Author: thebeast
 */


#if defined(__linux__)
#include <linux/limits.h>
#endif
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>

#include "libpkg.h"
#include "unixtools.h"

char* pkg_concat_str(int count, ...)
{
    va_list ap;
    ushort_t i;
    ushort_t len = 1;

    va_start(ap, count);
    for(i=0 ; i<count ; i++)
        len += strlen(va_arg(ap, char*));
    va_end(ap);

    char* merged = calloc(sizeof(char),len);
    ushort_t null_pos = 0;

    va_start(ap, count);
    for(i=0 ; i<count ; i++)
    {
        char* s = va_arg(ap, char*);
        strcpy(merged+null_pos, s);
        null_pos += strlen(s);
    }
    va_end(ap);

    return (char*)merged;
}

char* pkg_strip_repeat(char* str, char ch)
{
	  int rindex = 0, windex = 0 ;
	  char lchar = ' ' ;
	  while ( str[rindex] != '\0' ) {
	    if ( str[rindex] == ch ) {
	      if ( lchar != ch )
	        str[windex++] = str[rindex] ;
	    } else { str[windex++] = str[rindex] ; }
	    lchar = str[rindex++] ;
	  }
	  str[windex] = '\0' ;

	  return str ;
}

char* pkg_strip_spaces(char* str)
{
	size_t size;
	char *end;

	size = strlen(str);

	if (!size)
		return str;

	end = str + size - 1;
	while (end >= str && isspace(*end))
		end--;
	*(end + 1) = '\0';

	while (*str && isspace(*str))
    	str++;

	return str;
}

char* pkg_strip_trailing(char* str)
{
	size_t len = strlen(str);
	if( (len > 0) && (str[len-1] == '/') )
		str[len-1] = '\0';

	return str;
}

char* pkg_get_realpath(char* str)
{
	char buf[PATH_MAX + 1];
	char* ptr;

	ptr = realpath(str, buf);

	return (char*)ptr;
}

char* pkg_get_dirname(char* str)
{

	return EMPTY;
}

char* pkg_get_basename(char* str)
{

	return EMPTY;
}
