/*
 * unixhandlers.c
 *
 *  Created on: 16/03/2015
 *      Author: thebeast
 */



#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

#include "libpkg.h"
#include "unixhandlers.h"
#include "unixtools.h"

void destroy_path(path_t* path)
{
	free(path);
}

path_t* new_path(char* str)
{
	path_t* path = malloc(sizeof(path_t));

	path->rawinput = pkg_strip_trailing(pkg_strip_spaces(pkg_strip_repeat(str, '/')));
	path->exist = false;
	path->isafile = false;
	path->isadir = false;

	struct stat buf;
	int err = stat(path->rawinput, &buf);

	path->error = err;

	if( err == 0 )
	{
		path->exist = true;
		path->fullpath = pkg_get_realpath(path->rawinput);
		path->basename = pkg_get_basename(path->fullpath);
		path->dirname = pkg_get_dirname(path->fullpath);

		if( S_ISDIR(buf.st_mode) )
		{
			path->isadir = true;
			path->isafile = false;
		}

		if( S_ISREG(buf.st_mode) )
		{
			path->isafile = true;
			path->isadir = false;
		}
	} else {
		switch(errno)
		{
		case ENOENT :
			path->errmsg = "Not not found";
			break;
		case EACCES :
			path->errmsg = "Not access";
			break;
		}
	}

	return path;
}

unix_file_t* new_unix_file(char* rawpath)
{
	unix_file_t* file = malloc(sizeof(unix_file_t));

	file->rawpath = pkg_strip_trailing(pkg_strip_spaces(pkg_strip_repeat(rawpath, '/')));
	file->fullpath = pkg_get_realpath(file->rawpath);
	//file->dirname = pkg_get_dirname(file->fullpath);

	return file;
}
