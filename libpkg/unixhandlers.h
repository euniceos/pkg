/*
 * unixhandlers.h
 *
 *  Created on: 20/03/2015
 *      Author: thebeast
 */

#include <stdbool.h>

#include "list.h"

#ifndef UNIXHANDLERS_H_
#define UNIXHANDLERS_H_

typedef struct
{
	bool exist;
	bool isafile;
	bool isadir;
	int error;
	char* rawinput;
	char* fullpath;
	char* basename;
	char* dirname;
	char* errmsg;
} path_t;

typedef struct
{
	char* rawpath;
	char* fullpath;
	char* dirname;
	char* basename;
	bolean_t haveaccess;
	ushort_t permission;
	ushort_t size;
} unix_file_t;

typedef struct
{
	unix_file_t unix_file;

	list_t* content;
} unix_dir_t;

path_t* new_path(char* str);
void destroy_path(path_t* path);
unix_file_t* new_unix_file(char* rawpath);

#endif /* UNIXHANDLERS_H_ */
