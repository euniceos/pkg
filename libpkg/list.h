/*
 * list.h
 *
 *  Created on: 20/03/2015
 *      Author: thebeast
 */


#ifndef LIST_H_
#define LIST_H_

typedef struct
{
	size_t size;
	void** vector;
} list_t;

list_t* new_list();
ushort_t push_back(list_t* list, void* member);
size_t get_size(list_t* list);
void* get_position(list_t* list, size_t index);
int resize_list(list_t* list, size_t new_size);
void destroy_list(list_t* list);
void iter_list(list_t* list);

#endif /* LIST_H_ */
