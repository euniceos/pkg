/*
 * libpkg.h
 *
 *  Created on: 15/03/2015
 *      Author: thebeast
 */



#include <stdio.h>
#include <libintl.h>


#if defined(__linux__)
#include <linux/limits.h>
#include <sys/stat.h>
#endif

#ifndef _
#define _(String) gettext(String)
#endif

#ifndef LIBPKG_H_
#define LIBPKG_H_

#define RETURN_FAILURE		0
#define	RETURN_SUCCESS		1
#define REQUIRED			1
#define NOREQUIRED			0
#define DB_HOME				"/var/lib/pkg"
#define DB_FILE				"packages.db"
#define LOCALEDIR			"/usr/share/locale"
#define EMPTY				"empty"

typedef unsigned short int ushort_t;
typedef signed short int sshort_t;

typedef struct
{
	size_t size;
	char* str;
} string_t ;

typedef struct
{
	ushort_t sysarch;
	char* pkgsarch;
	ushort_t cpunum;
} system_t;

typedef enum
{
	NODEF,
	BIT32,
	BIT64,
	NOARCH
} arch_t;

typedef enum
{
	FALSE,
	TRUE
} bolean_t;

#endif /* LIBPKG_H_ */
